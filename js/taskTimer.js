/*************************
--------Task Timer--------
Version: 0.4
Author: Timofey Ponomarev 
*************************/

function correctTime(dig) {
	return ( dig < 10 ? "0" : "" ) + dig;
}


function currentTime() {

	//Получаем текущее время
	var currentTime	   = new Date(),
		currentHours   = currentTime.getHours(),
		currentMinutes = currentTime.getMinutes(),
		timeString	   = '';

	//Поправка на 00
	currentHours   = correctTime( currentHours );
	currentMinutes = correctTime( currentMinutes );

 	//Клеим строку
 	timeString = currentHours + ":" + currentMinutes;

 	//Обновляем время
 	document.getElementById("curTime").innerHTML = timeString; 
}


(function( $ ) {
	var nTimer = 1, //id нового таймера
		snd = new Audio("sound/tone.wav"); //звуковое уведомление после завершения


	$('#curTime').on('click', function() { 
		var hours      = this.innerHTML.substr(0,2),
			minutes    = this.innerHTML.substr(-2,2),
			endHour	   = $('.endTime input.h'),
			endMin     = $('.endTime input.m'),
			incrClick  = endMin.data('uClick') || 0,
			coorectHou = hours,
			correctMin = minutes;

		incrClick++;
		if((+minutes + (5 * incrClick)) >= 60) {
			correctMin = correctTime( (+minutes + (5 * incrClick) ) - 60 );
			coorectHou = correctTime( +coorectHou + 1 );
		}
		else {
			correctMin = +minutes + (5 * incrClick);
		}

		$('.startTime input.h').val(hours);
		$('.startTime input.m').val(minutes);
		endHour.val(coorectHou);

		incrClick >=6 ?
		endMin.val(correctMin).data('uClick', 0) :
		endMin.val(correctMin).data('uClick', incrClick);
	});

	$('#createTimer').click(function(e){
		e.preventDefault();

		var $this 		 = $(this),
			$pBlock 	 = $this.closest('.setup'),
			$timersBlock = $('.timersBlock'),
			title		 = $pBlock.find('#title').val(),
			invert		 = $pBlock.find('#checkInver').prop('checked'),
			addString	 = '';

		var currentTime	   = new Date(),
			currentHours   = currentTime.getHours(),
			currentMinutes = currentTime.getMinutes(),
			startH		   = +$pBlock.find('.startTime input').first().val(),
			startM		   = +$pBlock.find('.startTime input').last().val(),
			endH		   = +$pBlock.find('.endTime input').first().val(),
			endM	   	   = +$pBlock.find('.endTime input').last().val(),
			checkString    = correctTime(startH) + '' + correctTime(startM) + '-' + correctTime(endH) + '' + correctTime(endM),
			mask		   = /(0[0-9]|1[0-9]|2[0-3])[0-5][0-9]-(0[0-9]|1[0-9]|2[0-3])[0-5][0-9]/; ////[0-2][0-3][0-5][0-9]-[0-2][0-3][0-5][0-9]/;
		
		if( title.length == 0 )
			$pBlock.find('#title')
				   .addClass('error')
				   .delay(1000)
				   .queue(function() {
                           $(this).removeClass("error");
                           $(this).dequeue();
                   });

		if( mask.test(checkString) && (startH*60+startM <= endH*60+endM ) && title.length > 0 ) {

				invert ?
				addString = '<div id="uTime' + nTimer + '" class="timer br5px appended notActive invert"><div class="statusText wait">ожидание</div><h2 class="br5px">' + title + '</h2><div class="progressBar br5px"><span></span></div><div class="perc"><span class="zero timeBlock"><i title="Показать время" class="icon-time"></i><b>' + correctTime(startH) + ':' + correctTime(startM) + ' - ' + correctTime(endH) + ':' + correctTime(endM) + '</b></span><span class="hudr curPerc">100%</span><span class="centrBack"></span></div><a href="#" class="close">x</a></div>' :
				addString = '<div id="uTime' + nTimer + '" class="timer br5px appended notActive"><div class="statusText wait">ожидание</div><h2 class="br5px">' + title + '</h2><div class="progressBar br5px"><span></span></div><div class="perc"><span class="zero curPerc">0%</span><span class="hudr timeBlock"><i title="Показать время" class="icon-time"></i><b>' + correctTime(startH) + ':' + correctTime(startM) + ' - ' + correctTime(endH) + ':' + correctTime(endM) + '</b></span><span class="centrBack"></span></div><a href="#" class="close">x</a></div>';
				
				$timersBlock.prepend(addString)
							.children()
							.first()
							.animate({'margin-top' : 25}, 300, function() { 
								$(this).removeClass('appended'); 
							});
				
				var waitS = new Date().setTime(new Date().setHours(startH,startM,0,0));
					finS  = new Date().setTime(new Date().setHours(endH,endM,0,0));

				$('#uTime' + nTimer).anim_progressbar({ start: waitS, finish: finS });

				nTimer++;

				//Очистка полей
				$pBlock.find('input#title').val('');		
		}
	})

	$(document).on('click','.close', function(e){
		e.preventDefault();

		$(this).closest('.timer')
			 .animate({ 'opacity' : 0, 'top' : -100 }, 200 , function(){
			 	$(this).slideUp(300, function() { $(this).remove(); });
			 })
	});

	$(document).on(
	{
		click: function() { 
			$(this).siblings('b').toggleClass('active clicked'); 
		},
		mouseenter: function(){
			if( !$(this).siblings('b').hasClass('clicked') )
		    	$(this).siblings('b').addClass('active');
		},
		mouseleave: function(){
			if( !$(this).siblings('b').hasClass('clicked') )
		   		$(this).siblings('b').removeClass('active');
		}
	},'.icon-time');

	jQuery.fn.anim_progressbar = function (aOptions) {
        var iCms = 1000;
        var iMms = 60 * iCms;
        var iHms = 3600 * iCms;
        var iDms = 24 * 3600 * iCms;

        var aDefOpts = {
            start: new Date(),
            finish: new Date().setTime(new Date().getTime() + 60 * iCms),
            interval: 100
        }
        var aOpts   = jQuery.extend(aDefOpts, aOptions);
        var $vPb	= $(this),
			inverse = $vPb.hasClass('invert'),
        	invert  = 0;

        return this.each(
            function() {
                var iDuration = aOpts.finish - aOpts.start;

                var vInterval = setInterval(
                    function(){
                        var iLeftMs = aOpts.finish - new Date();
                        var iElapsedMs = new Date() - aOpts.start,
                            iDays = parseInt(iLeftMs / iDms),
                            iHours = parseInt((iLeftMs - (iDays * iDms)) / iHms),
                            iMin = parseInt((iLeftMs - (iDays * iDms) - (iHours * iHms)) / iMms),
                            iSec = parseInt((iLeftMs - (iDays * iDms) - (iMin * iMms) - (iHours * iHms)) / iCms),
                            iPerc = (iElapsedMs > 0) ? iElapsedMs / iDuration * 100 : 0; 


                        if( $vPb.find('.statusText').hasClass('wait') && iPerc > 0 ) {
		                	 $vPb.removeClass('notActive');
                        }

                        invert = ( 100 - iPerc );
                        
                        inverse ? 
                        $vPb.find('.curPerc')
                        	.html(invert.toFixed(1)+'%')
                        	.end()
                        	.find('.progressBar span')
                        	.css('width', invert.toFixed(1)+'%') : 
                        $vPb.find('.curPerc')
                        	.html(iPerc.toFixed(1)+'%')
                        	.end()
                        	.find('.progressBar span')
                        	.css('width', iPerc+'%');                                     

                        if (iPerc >= 100) {
                            clearInterval(vInterval);

                            inverse ? $vPb.find('.progressBar span').css('width', '0') : $vPb.find('.progressBar span').css('width', '100%');
                            inverse ? $vPb.addClass('successEndInv').find('.curPerc').html('0%') : $vPb.addClass('successEnd').find('.curPerc').html('100%');

                            snd.play();
                        }
                    } ,aOpts.interval
                );
            }
        );
    }

	$(function(){
		//Показываем текущее время и обновляем с интервалом в секунду
		currentTime();
		setInterval('currentTime()', 1000 );

	});

})( jQuery );